1)Domain classes have a relationship to the degree of relation they need to have.

2)We have added validation annotation for our core business feature and also our controller classes now enforce validation.

3)We have defined all the neccessary service classes for our core business feature.

4)We have modified our ER Diagram to contain relationship.

5)Controller methods now can handle get as well as post requests.

6)We have included test methods for all controller classes and methods.

7)We used trello for kanban board.we integrated the trello board with bitbucket,if it is not available we also have provided a screenshot of our kanban board on the repository.Finally we also added you to the project trello board.

8)We have corrected the package name to follow java's naming convention.

